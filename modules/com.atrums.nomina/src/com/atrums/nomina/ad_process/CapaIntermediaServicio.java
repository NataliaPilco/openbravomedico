package com.atrums.nomina.ad_process;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.database.ConnectionProviderImpl;
import org.openbravo.exception.PoolNotFoundException;

import com.atrums.nomina.util.UtilNomina;

public class CapaIntermediaServicio extends Thread {
  private static final Logger log = Logger.getLogger(CapaIntermediaServicio.class);
  NOCBPartner partner = null;

  private String idServicioLinea = "";

  public Integer obtenerValidacion() throws Exception {
    return partner.permitirEnviarMail();
  }

  public boolean terceroPagoServicio(String idServicioLinea) throws ServletException {
    NOCBPartnerData[] datos = partner.mailPagosServicios(idServicioLinea);
    if (datos == null || datos.length >= 1) {
      return true;
    } else {
      return false;
    }
  }

  public CapaIntermediaServicio() throws PoolNotFoundException {
    String strDirectorio = UtilNomina.class.getResource("/").getPath();
    Integer value = strDirectorio.indexOf("/src-core/build/classes/");
    try {
      if (value > 0) {
        strDirectorio = strDirectorio.substring(1, value + 1);
        strDirectorio = strDirectorio + "config/";
      } else {
        value = strDirectorio.indexOf("/build/classes/");
        if (value > 0) {
          strDirectorio = strDirectorio.substring(1, value + 1);
          strDirectorio = strDirectorio + "config/";
        } else {
          value = strDirectorio.indexOf("/WEB-INF/classes/");
          log.info("strDirectorio " + value);
          if (value > 0) {
            strDirectorio = strDirectorio.substring(1, value + 1);
            strDirectorio = "/" + strDirectorio + "" + "WEB-INF/";
            log.info(strDirectorio);
          } else {
            log.info("Problema en el Path " + value);
            strDirectorio = "/var/lib/tomcat6/webapps/openbravo/WEB-INF/";
          }
        }
      }
    } catch (Exception e) {
      log.error(e.toString() + " --------------" + strDirectorio);
    }
    final ConnectionProvider conn = new ConnectionProviderImpl(strDirectorio
        + "/Openbravo.properties");
    partner = new NOCBPartner(conn);
  }

  @SuppressWarnings("static-access")
  public void enviarMails(String idRolPago) {
    try {
      partner.enviarMail(partner.obtenerMailTerceros(idRolPago));
    } catch (ServletException e) {
      // TODO Auto-generated catch block
      log.error(e.toString());
    } catch (Exception e) {
      // TODO Auto-generated catch block
      log.error(e.toString());
    }
  }

  @SuppressWarnings("static-access")
  public void enviarMails() {
    try {
      if (partner.obtenerMailTerceros(getIdServicioLinea()).length > 0) {
        partner.enviarMail(partner.obtenerMailTerceros(getIdServicioLinea()));
      } else {
        partner.enviarMailPagoServicio(partner.mailPagosServicios(getIdServicioLinea()));
      }

    } catch (ServletException e) {
      // TODO Auto-generated catch block
      log.error(e.toString());
    } catch (Exception e) {
      // TODO Auto-generated catch block
      log.error(e.toString());
    }
  }

  public void run() {
    enviarMails();
  }

  public String getIdServicioLinea() {
    return idServicioLinea;
  }

  public void setIdServicioLinea(String idServicioLinea) {
    this.idServicioLinea = idServicioLinea;
  }

}
